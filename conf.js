const conf = {
    title : "Gazzetta dell'OSR",
    port : process.env.PORT || 3000,
    url : "https://gazzetta.oicn.icu/",
    intro : "Una <a href=\"https://it.wiktionary.org/wiki/crestomazia\">crestomazia</a> della blogosfera italiana. Compilata e aggiornata automaticamente.",
    meta: {
        title : "Gazzetta dell'OSR",
        description : "Una crestomazia della blogosfera italiana. Compilata e aggiornata automaticamente.",
        keywords : "osr, gdr, giochi di ruolo, blog, blogosfera"
    },
    about: [
        {
            title: "",
            text : [
                "La Gazzetta si ispira direttamente al lavoro di Alex Schroeder con il suo <a href=\"https://campaignwiki.org/osr/\">Old School RPG Planet</a>. L'idea è di compilare una lista dei blog OSR italiani che si possa trovare in un unico posto. Se hai un blog OSR in italiano, sei il benvenuto qui dentro.",
                "Il nostro scopo è di dare vita a una blogosfera italiana vivace e attiva come la sua controparte inglese. Avere più blog e discussioni sull'OSR, più materiale italiano e, in generale, una comunità bella numerosa e attiva. Se vuoi partecipare (e non hai un blog) puoi cominciare con una qualsiasi delle piattaforme gratuite si trovano in giro: Blogger, Wordpress, Tumblr e via dicendo. Oppure, se sei interessato a sperimentare, avevo fatto una guida ad altri modi di mettersi in piedi un blog: <a href=\"http://oicn.icu/2020/Come-tirare-su-il-vostro-blog/\">Come tirare su il vostro blog</a>."
            ]
        },
        {
            title: "Come leggerci",
            text: [
                "Il modo più facile di leggere la Gazzetta è di passare di qua una o due volte al giorno. Il sito si aggiorna periodicamente (ogni blog viene letto, all'incirca, una volta ogni 4 ore) e i nuovi post appaiono in home.",
                "Se invece sei interessato a leggerci senza tornare qua tutti i giorni, ci sono due modi:<ul><li>Abbiamo un <a href=\"/feed/rss.xml\">feed RSS</a> (ironico, non trovi?) che raccoglie tutto quello che viene mostrato in home; puoi usare il tuo lettore RSS preferito.</li><li>C'è un <a href=\"https://t.me/gazzetta_osr\">canale Telegram</a>, che pesca direttamente dal feed</li></ul>"
            ]
        }
    ],
    faq : [
        {
            q: "Cos'è questo sito?",
            answer : [
                "La Gazzetta dell'OSR è un blogroll o pianeta dei blog OSR italiani. Legge periodicamente i feed RSS dei blog presenti e compila una sola pagina con tutti i loro post, in ordine cronologico.",
                "Se torni spesso a visitare questa pagina, troverai post diversi. Lo storico viene sempre conservato."
            ]
        },
        {
            q: "Ho un blog, posso partecipare anch'io?",
            answer: [
                "Certamente, mandami un messaggio su Telegram a <a href=\"https://t.me/oi_cn_bot\">@oi_cn_bot</a> e ti aggiungerò il prima possibile."
            ]
        },
        {
            q: "Voglio fare una cosa simile, come posso fare?",
            answer : [
                "C'è un <a href=\"https://gitlab.com/frollo/my-own-planet\">repository su Gitlab</a>, ancora in forma un po' rozza, da cui puoi partire.",
                "La licenza è completamente libera, quindi facci quello che vuoi, ma ci vorrà ancora un po' di lavoro perché sia immediatamente usabile. Se hai dubbi o problemi, ci sono gli issue."
            ]
        },
        {
            q: "Il mio blog è presente su questo sito, ma vorrei rimuoverlo",
            answer : [
                "Mi spiace che tu voglia lasciarci, ma non c'è alcun problema. Contattami su Telegram a <a href=\"https://t.me/oi_cn_bot\">@oi_cn_bot</a> e ti rimuoverò appena possibile."
            ]
        },
        {
            q: "Ho trovato qualcosa che non dovrebbe davvero essere qui, che faccio?",
            answer: [
                "Ahia. Questo è uno dei problemi dell'aggiornamento automatico: niente filtri. Fammi un favore, avvisami su Telegram a <a href=\"https://t.me/oi_cn_bot\">@oi_cn_bot</a>, con il link incriminato e mi inventerò un modo di non farlo apparire. Se ti interessa, posso anche mandarti, minuto per minuto, tutte le mie imprecazioni mentre ci lavoro. Qualcuno ci si diverte."
            ]
        }
    ]
}

module.exports = conf;
