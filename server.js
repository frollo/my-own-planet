const express = require('express');
const basicAuth = require('express-basic-auth');
var bodyParser = require('body-parser');
const pug = require('pug');
const mysql = require('mysql');
const moment = require('moment');
const RSS = require("rss");
const conf = require("./conf");
const worker = require("./worker");
moment.locale('it');


function auth(username, password){
    const userMatches = basicAuth.safeCompare(username, process.env.ADMIN_USER || "test");
    const passwordMatches = basicAuth.safeCompare(password, process.env.ADMIN_PASSWORD || "test");
    return userMatches && passwordMatches;
}

const pool = mysql.createPool(process.env.CLEARDB_DATABASE_URL);
const app = express();
//Update feeds
worker();

app.locals.title = conf.title;
app.locals.meta = conf.meta;
app.locals.url = conf.url;
app.locals.questions = conf.faq;
app.locals.about = conf.about;
app.locals.intro = conf.intro;

function manage(res){
    pool.query("SELECT * FROM blogs", (err, result) => {
        if(err){
            res.render('err', { title: conf.title,content : err});
        }else{
            const prettifiedResults = result.map(r => {
                return {
                    name : r.name,
                    homeLink : r.homeLink,
                    feedLink : r.feedLink,
                    lastUpdate : moment(r.lastUpdate)
                }
            })
            res.render('manage', {title : conf.title, blogs : prettifiedResults});
        }
    })
}

function compileFeed(){
    var feed = new RSS({
        title : conf.title,
        description: conf.intro,
        feed_url : conf.url + "/feed",
        site_url : conf.url,
        language : "it",
        ttl: 30
    });
    return new Promise((resolve, reject) =>{
        pool.query("SELECT title, link, excerpt, publication, blogs.name AS blogName FROM posts JOIN blogs ON posts.blog = blogs.id ORDER BY publication DESC LIMIT 50", (err, result) => {
            if(err){
                reject(err);
            }else{
                result.forEach(post => {
                    feed.item(
                        {
                            title : post.title,
                            description : post.excerpt,
                            url : post.link,
                            author : post.blogName,
                            date : post.publication
                        }
                    )
                });
                resolve(feed);
            }
        })
    });
}

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'pug');

app.use("/style.min.css", express.static('node_modules/latex.css/style.min.css'));
app.use("/fonts", express.static("node_modules/latex.css/fonts"))
app.use("/static", express.static("static"));

app.get("/blogs", function(req, res){
    pool.query("SELECT name, homeLink FROM blogs", (err, result) =>{
        if(err){
            res.render('err', {content:err});
        }else{
            res.render('blogs', {blogs : result});
        }
    });
});

app.use("/manage", basicAuth({
    authorizer : auth,
    challenge : true
}));

app.get("/manage", function(req, res){
    manage(res);
});

app.post("/manage", (req, res)=>{
    var newBlog = req.body;
    if(newBlog.name && newBlog.feedLink && newBlog.homeLink){
        pool.query("INSERT INTO blogs(name,homeLink,feedLink) VALUES (?,?,?)", [newBlog.name, newBlog.homeLink, newBlog.feedLink], (err, _) =>{
            if(err){
                res.render("err", {content:err});
            }else{
                manage(res);
            }
        });
    }else{
        manage(res);
    }
});

app.get("/faq", (_, res) => {
    res.render("faq", {});
});

app.get("/about", (_, res) =>{
    res.render("about", {});
})

app.get("/feed/rss.xml", (_,res) =>{
    compileFeed()
        .then(feed => {
            res.set('Content-Type', 'application/rss+xml');
            res.send(feed.xml({indent : true}));
        })
        .catch(err => {
            res.render('err', {content: err});
        })
})

app.get('/:pagenumber?', function (req, res) {
    var pageNumber = parseInt(req.params.pagenumber) || 0;
    pool.query("SELECT title, link, excerpt, publication, blogs.name AS blogName, blogs.homeLink AS homeLink FROM posts JOIN blogs ON posts.blog = blogs.id ORDER BY publication DESC LIMIT 20 OFFSET ?", 20 * pageNumber,(err, results) => {
        if(err){
            res.render('err', {content : err});
        }else{
            var prettyfiedResults = results.map(r => {
                return {
                    title : r.title,
                    link : r.link,
                    excerpt : r.excerpt,
                    publication : moment(r.publication),
                    blogName : r.blogName,
                    homeLink : r.homeLink
                }
            })
            res.render('index', {posts: prettyfiedResults, prevPage : pageNumber - 1, nextPage: pageNumber + 1});
        }
    });
});

app.listen(conf.port, () => console.log(`Example app listening at http://localhost:${conf.port}`));
