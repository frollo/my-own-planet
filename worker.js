const mysql = require('mysql');
const Parser = require('rss-parser');
const truncate = require('truncate-html');
const moment = require('moment');

const parser = new Parser();
const pool = mysql.createPool(process.env.CLEARDB_DATABASE_URL);

function parseDate(d){
    var m = new moment(d);
    return m.format();
}

function updateFeeds(){
    pool.query("SELECT * FROM blogs WHERE lastUpdate < (CURRENT_TIMESTAMP() - INTERVAL 4 HOUR)", (error, result) => {
        if(error){
            console.log(err);
        }else{
            console.log(result);
            if(result.length === 0){
                console.log("No feeds to update");
            }else{
                result.forEach((blog, index, array) => {
                    parser.parseURL(blog.feedLink)
                    .then((output) => {
                        console.log(`Index ${index}\tArray ${array.length}`);
                        const toInsert = output.items.map( p => [blog.id, p.title, p.link, parseDate(p.pubDate), truncate(p.contentSnippet || "", 50, {stripTags : true, byWords : true})]);
                        pool.query("INSERT IGNORE INTO posts (blog, title, link, publication, excerpt) VALUES ?", [toInsert], (err,_) =>{
                            if(err){
                                console.log(`Error updating posts for blog ${blog.title}: ${err}`);
                            }

                            pool.query("UPDATE blogs SET lastUpdate = CURRENT_TIMESTAMP() WHERE id = ?", blog.id, (err,_) => {
                                if(err){
                                    console.log(`Error updating last read time for ${blog.title}`);
                                }
                            });
                        });
                    });   
                })
            }
        }
    })

}

module.exports = updateFeeds;